package transport

import (
	"context"
	"gitlab.com/cursol-pub/log"
)

// ErrorHandler is a transport error handler implementation which logs an error.
type ErrorHandler struct {
	logger log.Logger
}

func NewErrorHandler(logger log.Logger) *ErrorHandler {
	return &ErrorHandler{
		logger: logger,
	}
}

func (h *ErrorHandler) Handle(ctx context.Context, err error) {
	h.logger.Log(ctx, "error occurred", "err", err)
}
