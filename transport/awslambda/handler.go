package lmb

import (
	"context"
	"encoding/json"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/lambda"
	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/transport"
	"github.com/go-kit/kit/transport/awslambda"
	"github.com/go-kit/log"
)

type EncodeRequestFunc[T any] func(context.Context, *T) ([]byte, error)

type DecodeResponseFunc[R any] func(context.Context, []byte) (R, error)

type optionSetter interface {
	setErrorHandler(eh transport.ErrorHandler)
	setErrorEncoder(ee awslambda.ErrorEncoder)
}

type HandlerOption func(os optionSetter)

func HandlerErrorHandler(eh transport.ErrorHandler) HandlerOption {
	return func(os optionSetter) { os.setErrorHandler(eh) }
}

func HandlerErrorEncoder(ee awslambda.ErrorEncoder) HandlerOption {
	return func(os optionSetter) { os.setErrorEncoder(ee) }
}

func defaultErrorEncoder(_ context.Context, err error) ([]byte, error) {
	return nil, err
}

type Handler[T, R any] struct {
	client       *lambda.Client
	function     string
	enc          EncodeRequestFunc[T]
	dec          DecodeResponseFunc[R]
	errorEncoder awslambda.ErrorEncoder
	errorHandler transport.ErrorHandler
}

func (h *Handler[T, R]) setErrorHandler(eh transport.ErrorHandler) {
	h.errorHandler = eh
}

func (h *Handler[T, R]) setErrorEncoder(ee awslambda.ErrorEncoder) {
	h.errorEncoder = ee
}

func NewExplicitHandler[T, R any](client *lambda.Client, function string,
	enc EncodeRequestFunc[T], dec DecodeResponseFunc[R], options ...HandlerOption) *Handler[T, R] {
	h := &Handler[T, R]{
		client:       client,
		function:     function,
		enc:          enc,
		dec:          dec,
		errorEncoder: defaultErrorEncoder,
		errorHandler: transport.NewLogErrorHandler(log.NewNopLogger()),
	}

	for _, option := range options {
		option(h)
	}

	return h
}

func NewHandler[T, R any](client *lambda.Client, function string, options ...HandlerOption) *Handler[T, R] {
	return NewExplicitHandler[T, R](client, function, nil, nil, options...)
}

func (h *Handler[T, R]) Endpoint() endpoint.Endpoint {
	if h.enc == nil {
		h.enc = EncodeRequest[T]
	}
	if h.dec == nil {
		h.dec = DecodeResponse[R]
	}
	return func(ctx context.Context, req any) (resp any, err error) {
		ctx, cancel := context.WithCancel(ctx)
		defer cancel()

		enc, err := h.enc(ctx, req.(*T))
		if err != nil {
			h.errorHandler.Handle(ctx, err)
			return h.errorEncoder(ctx, err)
		}

		params := lambda.InvokeInput{
			FunctionName: aws.String(h.function),
			Payload:      enc,
		}

		output, err := h.client.Invoke(ctx, &params)
		if err != nil {
			h.errorHandler.Handle(ctx, err)
			return h.errorEncoder(ctx, err)
		}

		resp, err = h.dec(ctx, output.Payload)
		if err != nil {
			h.errorHandler.Handle(ctx, err)
			return h.errorEncoder(ctx, err)
		}

		return
	}
}

func EncodeRequest[T any](_ context.Context, req *T) (resp []byte, err error) {
	resp, err = json.Marshal(&req)
	if err != nil {
		return
	}

	return
}

func DecodeResponse[R any](_ context.Context, r []byte) (resp R, err error) {
	if err = json.Unmarshal(r, &resp); err != nil {
		return
	}

	return
}
