module gitlab.com/cursol-pub/go-kit

go 1.21.1

require (
	github.com/aws/aws-sdk-go-v2 v1.21.0
	github.com/aws/aws-sdk-go-v2/service/lambda v1.39.5
	github.com/go-kit/kit v0.13.0
	github.com/go-kit/log v0.2.1
	gitlab.com/cursol-pub/log v0.1.0
)

require (
	github.com/aws/aws-sdk-go-v2/aws/protocol/eventstream v1.4.13 // indirect
	github.com/aws/aws-sdk-go-v2/internal/configsources v1.1.41 // indirect
	github.com/aws/aws-sdk-go-v2/internal/endpoints/v2 v2.4.35 // indirect
	github.com/aws/smithy-go v1.14.2 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-logfmt/logfmt v0.6.0 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
)
